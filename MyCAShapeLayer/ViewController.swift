//
//  ViewController.swift
//  MyCAShapeLayer
//
//  Created by HellöM on 2019/12/5.
//  Copyright © 2019 HellöM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var artboardView: ArtboardView!
    var colorView: UIStackView!
    var slider: UISlider!
    var clearBtn: UIButton!
    var backBtn: UIButton!
    var timer: Timer!
    var eraserBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        initUI()
    }
    
    override var prefersStatusBarHidden: Bool {
        
        return true
    }
    
    func initUI() {
        
//        let height = UIApplication.shared.windows[0].windowScene!.statusBarManager!.statusBarFrame.height
        
        artboardView = ArtboardView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.size.height))
        artboardView.clipsToBounds = true
        artboardView.isMultipleTouchEnabled = false
        artboardView.layer.borderWidth = 1
        artboardView.layer.cornerRadius = 10
        artboardView.delegate = self
        
        view.addSubview(artboardView)
        
        clearBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        clearBtn.center = CGPoint(x: view.frame.size.width/4, y: 45)
        clearBtn.setTitle("清除", for: .normal)
        clearBtn.addTarget(self, action: #selector(clearClick), for: .touchUpInside)
        
        view.addSubview(clearBtn)
        
        backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        backBtn.center = CGPoint(x: view.frame.size.width*0.75, y: 45)
        backBtn.setTitle("返回", for: .normal)
        backBtn.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        
        view.addSubview(backBtn)
        
        colorView = UIStackView(frame: CGRect(x: 10, y: self.view.frame.size.height-70, width: self.view.frame.size.width-20, height: 40))
        colorView.distribution = .equalSpacing
        colorView.axis = .horizontal
        
        view.addSubview(colorView)
        
        for index in 1...7 {
            
            let colorBtn = UIButton()
            colorBtn.clipsToBounds = true
            colorBtn.layer.cornerRadius = 20
            colorBtn.layer.borderWidth = 1
            colorBtn.layer.borderColor = UIColor.white.cgColor
            colorBtn.tag = 100+index
            colorBtn.addTarget(self, action: #selector(colorClick(colorBtn:)), for: .touchUpInside)
            
            let widthConstraint = NSLayoutConstraint.init(item: colorBtn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            widthConstraint.isActive = true
                    
            let heightConstraint = NSLayoutConstraint.init(item: colorBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            heightConstraint.isActive = true
            
            switch index {
            case 1:
                colorBtn.backgroundColor = .red
            case 2:
                colorBtn.backgroundColor = .orange
            case 3:
                colorBtn.backgroundColor = .yellow
            case 4:
                colorBtn.backgroundColor = .green
            case 5:
                colorBtn.backgroundColor = .blue
            case 6:
                colorBtn.backgroundColor = .gray
            case 7:
                colorBtn.backgroundColor = .purple
            default:
                colorBtn.backgroundColor = .white
            }
            
            colorView.addArrangedSubview(colorBtn)
        }
        
        eraserBtn = UIButton(frame: CGRect(x: colorView.frame.origin.x+5, y: colorView.frame.origin.y-45, width: 40, height: 38))
        eraserBtn.setBackgroundImage(UIImage.init(named: "Eraser"), for: .normal)
        eraserBtn.addTarget(self, action: #selector(eraserClick), for: .touchUpInside)
        
        view.addSubview(eraserBtn)
        
        slider = UISlider(frame: CGRect(x: -125, y: 300, width: 250, height: 20))
        slider.minimumValue = 3
        slider.maximumValue = 50
        
        slider.maximumTrackTintColor = UIColor.gray
        slider.value = 15
        slider.addTarget(self,action:#selector(sliderTouchUpInside), for:.touchUpInside)
        slider.addTarget(self,action:#selector(sliderTouchUpOutside), for:.touchUpOutside)
        slider.addTarget(self,action:#selector(sliderTouchDown(_:)), for:.touchDown)
        slider.addTarget(self,action:#selector(sliderDidchange(_:)), for:.valueChanged)
        slider.transform = CGAffineTransform.init(rotationAngle: -.pi/2)
        view.addSubview(slider)
    }
    
    @objc
    func eraserClick() {
        
        artboardView.eraserHandler()
    }
    
    @objc
    func sliderTouchUpInside() {
        
        artboardView.clearColorDemo()
        
        UIView.animate(withDuration: 0.5) {
            
            self.slider.frame.origin.x = -10
        }
    }
    
    @objc
    func sliderTouchUpOutside() {
        
        artboardView.clearColorDemo()
        
        UIView.animate(withDuration: 0.5) {
            
            self.slider.frame.origin.x = -10
        }
    }
    
    @objc
    func sliderTouchDown(_ slider: UISlider) {
    
        artboardView.sliderTouchDown()
        
        UIView.animate(withDuration: 0.5) {
            
            self.slider.frame.origin.x = 10
        }
    }
    
    @objc
    func sliderDidchange(_ slider: UISlider) {
        
        artboardView.fixLineWidth(width: CGFloat(slider.value))
    }
    
    @objc
    func colorClick(colorBtn: UIButton) {
        
        let index = colorBtn.tag-100
        let color: UIColor!
        
        switch index {
        case 1:
            color = .red
        case 2:
            color = .orange
        case 3:
            color = .yellow
        case 4:
            color = .green
        case 5:
            color = .blue
        case 6:
            color = .gray
        case 7:
            color = .purple
        default:
            color = .white
        }
        
        artboardView.lineColor = color
    }
    
    @objc
    func clearClick() {
        
        let alert = UIAlertController(title: "注意", message: "確定要清除嗎?", preferredStyle: .actionSheet)
        
        let determine = UIAlertAction(title: "確定", style: .destructive) { (_) in
            
            self.artboardView.clearView()
        }
        let cencle = UIAlertAction(title: "保留", style: .default, handler: nil)
        
        alert.addAction(determine)
        alert.addAction(cencle)
        
        
        
        present(alert, animated: false, completion: nil)
    }
    
    @objc
    func backClick() {
        
        artboardView.backHandler()
    }
}

extension ViewController: ArtboardViewDelegate {
    
    func artboardBegan() {
        
        if timer != nil {
            
            timer.invalidate()
        }
        
        UIView.animate(withDuration: 0.4) {
            
            self.hideUI()
        }
    }
    
    func artboardEnded() {
        
        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: { (_) in
            UIView.animate(withDuration: 0.4) {
                self.showUI()
            }
        })
    }
    
    func hideUI() {
        
        slider.alpha = 0
        clearBtn.alpha = 0
        backBtn.alpha = 0
        colorView.alpha = 0
        eraserBtn.alpha = 0
    }
    
    func showUI() {
        
        slider.alpha = 1
        clearBtn.alpha = 1
        backBtn.alpha = 1
        colorView.alpha = 1
        eraserBtn.alpha = 1
    }
}

