//
//  ArtboardView.swift
//  MyCAShapeLayer
//
//  Created by HellöM on 2019/12/5.
//  Copyright © 2019 HellöM. All rights reserved.
//

import UIKit

protocol ArtboardViewDelegate {
    
    func artboardBegan()
    func artboardEnded()
}

class ArtboardView: UIView {

    
    var lineWith: CGFloat = 15
    var startPoint: CGPoint!
    var touchPoint: CGPoint!
    var path: UIBezierPath!
    var baseView: UIView!
    var pathAry: NSMutableArray!
    var allPathAry: NSMutableArray!
    var delegate: ArtboardViewDelegate?
    var colorSize: UIBezierPath!
    var colorSizeLayer: CAShapeLayer!
    var colorBaseLayer: CAShapeLayer!
    var isEraser: Bool = false
    
    
    var lineColor = UIColor.red {
        didSet {
            isEraser = false
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        pathAry = NSMutableArray()
        allPathAry = NSMutableArray()
        path = UIBezierPath()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func sliderTouchDown() {
        
        colorSize = UIBezierPath()
        colorSizeLayer = CAShapeLayer()
        colorBaseLayer = CAShapeLayer()
        
        colorSize.move(to: self.center)
        colorSize.addLine(to: self.center)
        
        colorBaseLayer.path = colorSize.cgPath
        colorBaseLayer.lineWidth = lineWith+3
        colorBaseLayer.fillColor = UIColor.clear.cgColor
        colorBaseLayer.lineCap = .round
        self.layer.addSublayer(colorBaseLayer)
        
        colorSizeLayer.path = colorSize.cgPath
        colorSizeLayer.lineWidth = lineWith
        colorSizeLayer.fillColor = UIColor.clear.cgColor
        colorSizeLayer.lineCap = .round
        self.layer.addSublayer(colorSizeLayer)
        
        UIView.animate(withDuration: 0.4) {
        
            if self.isEraser {
                self.colorSizeLayer.strokeColor = UIColor.black.cgColor
            } else {
                self.colorSizeLayer.strokeColor = self.lineColor.cgColor
            }
            self.colorBaseLayer.strokeColor = UIColor.white.cgColor
            
        }
    }
    
    func eraserHandler() {
        
        isEraser = true
    }
    
    func clearColorDemo() {
        
        UIView.animate(withDuration: 0.4) {
            
            self.colorBaseLayer.strokeColor = UIColor.clear.cgColor
            self.colorSizeLayer.strokeColor = UIColor.clear.cgColor
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        delegate?.artboardBegan()
        
        startPoint = touches.first?.location(in: self)
        pathAry.removeAllObjects()
        
        draw(move: startPoint, addLine: startPoint, isEraser: isEraser)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        touchPoint = touches.first?.location(in: self)
        
        
        draw(move: startPoint, addLine: touchPoint, isEraser: isEraser)
        
        startPoint = touchPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard  touches.first != nil else {
            return
        }
        
        delegate?.artboardEnded()
        allPathAry.add(pathAry!.copy())
    }
    
    func draw(move: CGPoint, addLine: CGPoint, isEraser: Bool) {
        
        path = UIBezierPath()
        path.move(to: move)
        path.addLine(to: addLine)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = lineWith
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = .round
        
        if isEraser {
            
            shapeLayer.strokeColor = UIColor.black.cgColor
            
        } else {
            shapeLayer.strokeColor = lineColor.cgColor
        }
        self.layer.addSublayer(shapeLayer)
        self.setNeedsDisplay()
        
        pathAry.add(shapeLayer)
    }
    
    func clearView() {
        
        path.removeAllPoints()
        layer.sublayers = nil
        setNeedsDisplay()
    }
    
    func backHandler() {
        
        if allPathAry.count == 0 { return }
        
        let pathAry = allPathAry!.lastObject as! NSArray
        
        for shapeLayer in pathAry {
            
            let shapeLayer = shapeLayer as! CAShapeLayer
            shapeLayer.removeFromSuperlayer()
        }
        
        allPathAry.removeLastObject()
    }
    
    func fixLineWidth(width: CGFloat) {
        
        lineWith = width
        colorBaseLayer.lineWidth = width+3
        colorSizeLayer.lineWidth = width
    }
}
